# Mau kontribusi?
Makasih mau ngekontribusi ke projek ini,ini guideline biar request mu gak hancur  
Projek ini memakai banyak markdown,jadi liat dulu sebelum mencoba  

dah tau semua? ok,ayuk kita lanjut

## Make pull request
1. cek dulu sebelum request,karna pr mu bisa ada misspelling,hyperlink fail,dan eror markdown
2. link-kan sumber di deskripsi pull request jika menambah informasi
3. jangan lupa kamu masih manusia,masalah selalu ada,tapi saya bisa perbaiki untukmu jadi gak apa2
4. link-kan juga medsos mu di deskripsi pr (opsional)  
love you ;P

# Guide

### Spasi
Spasinya gini gan
```
text1  <- ada 2 spasi
text2
```

# Template

#### DNS
`| [DNS](DNS Site) | note | ? | ? | ? | ? | ? | ? |`

```
| DNS | note | `ipv4` | X | `ipv6` | X | `doh` | `dot` |
```

#### VPN
`| [VPN](VPN Site) | Positif | Negatif | Jumlah Server |`

Advanced VPN  
`| [VPN](VPN Site) | Deskripsi |`

#### Aplikasi
```
Number.[App](Situs App) [Platform]  
deskripsi
```

#### [kredit.md](/kredit.md)
```
| <a href="link sosial utama"><img src="link foto" width="50px" /><a/> | kontribusi 1<br />kontibusi 2(optional) | link sosial,pakai template tombol dibawah |
```

Template tombol:  
```
<a href="link medsos mu"><img src="taruh link img.shields.io disini"/><a/>
```
cari link shields [disini](https://github.com/alexandresanlim/Badges4-README.md-Profile)

## Cara kontribusi memakai issue/discord

Kalo gak tau cara make pull req,kamu bisa pakai issue atau server discord bebasid  
kasih tau aplikasi atau informasi apa yang mau di tambah dan link sumber sekalian medsos mu,kalau pake server discord bebasid ping saya (user: .Me.)
<p align="center">
    <a href="https://discord.gg/EKrxZyu"><img src="https://discordapp.com/api/guilds/630415907021389825/widget.png?style=banner4" alt="Discord Banner 4"/></a>
