# Kontributor

| Terima | Kasih | Semua |
| :------: | :-----: | :-----: |
| <a href="https://twitter.com/mefinity"><img src="https://github.com/MeFinity.png" width="50px" /><a/> | Pembuat KominFudge |  <a href="https://github.com/MeFinity"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/><a/> <a href="https://twitter.com/mefinity"><img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white"/><a/><br /><a href="https://t.me/MeFinity"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/><a/> <a href="https://reddit.com/u/Me_Finity"><img src="https://img.shields.io/badge/Reddit-FF4500?style=for-the-badge&logo=reddit&logoColor=white"/> |
| <a href="https://example.com"><img src="https://github.com/ghost.png" width="50px" /><a/> | Provider data ISP<br />Update tutorial DNS<br />Provider data DPI ISP<br />Kebanjiran fakta 😭 | No Referer |
| <a href="https://github.com/lepz0r"><img src="https://github.com/lepz0r.png" width="50px" /><a/> |  Suggest VPN + DNS<br />Suggest ganti Layout DNS<br />Update tutorial<br />Memberi informasi DoH<br />Pembuat [dpi-circumvention-config](/dpi-circumvention-config) | <a href="https://github.com/lepz0r"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/><a/> <a href="https://twitter.com/CincnMskMangkok"><img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white"/><a/> <a href="https://t.me/CincinMasukMangkok"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/><a/> |
| <a href="https://t.me/ZeroExa"><img src="https://cdn.discordapp.com/attachments/973116913045602334/1007228548194517032/ZE.jpg" width="50px" /><a/> | Suggest DNS + VPN<br />Suggest Aplikasi DNS<br />Memberi lebih banyak informasi DoH (curl) | <a href="https://github.com/ZeroExa"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/><a/> <a href="https://t.me/ZeroExa"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/><a/> |
| <a href="https://t.me/andreas_ding2"><img src="https://cdn.discordapp.com/attachments/973116913045602334/1007236497310765116/AD.jpg" width="50px" /><a/> | Suggest aplikasi DNS<br />Suggest VPN | <a href="https://t.me/andreas_ding2 "><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/><a/> |
| <a href="https://github.com/RacBallonMC"><img src="https://github.com/RacBallonMC.png" width="50px" /><a/> | Fix cloudflare DNS | <a href="https://github.com/RacBallonMC"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/><a/> |
| <a href="https://github.com/neneeen"><img src="https://github.com/neneeen.png" width="50px" /><a/> | Update DNS | <a href="https://github.com/neneeen"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/><a/> |

# Situs berguna
 
 1. https://github.com/bebasid/bebasid - dokumen-dokumen berguna
 2. https://privacytools.io - Provider data VPN + Aplikasi DNS
 3. https://github.com/alexandresanlim/Badges4-README.md-Profile - tombol link (note: https://shields.io )
 4. https://www.sharelinkgenerator.com - share link

### <p align="center">Share projek ini</p>
<div id="sosial">
 <p align="center">
  <a href="https://twitter.com/intent/tweet?text=https%3A//github.com/MeFinity/KominFudge%20%23BlokirKominfo%20%23BlokirGakPakeMikir"><img src="https://img.shields.io/badge/Twitter-blue?style=flat&logo=twitter&logoColor=white"/></a>
  <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//github.com/MeFinity/KominFudge"><img src="https://img.shields.io/badge/Facebook-1877F2?style=flat&logo=facebook&logoColor=white"/></a>
 </p>
</div>
