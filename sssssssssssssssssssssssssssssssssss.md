# Pegawai Kominfo gak boleh disini
pergi lu kamvret :v

## ISP

| Nama | Pemblokiran menggunakan DNS | Pemblokiran menggunakan DPI |
| :---: | :---: | :---: |
| Indihome | DNS Nasional | Ya |
| CBN | DNS Nasional | Tidak |
| Biznet | DNS Nasional | Tidak |
| MyRepublic | DNS Nasional | Tidak |
| FirstMedia | DNS Nasional | Tidak |
| Megavision | DNS Nasional | Tidak |
| MNC | DNS | Tidak |
| PT Remala Abadi | DNS | Tidak |
| PT iForte Global internet | DNS | Tidak |
| Iconnet PLN | DNS | Ya |
| Telkomsel | DNS Nasional | Ya |
| XL | DNS Nasional | Ya |
| 3 | DNS Nasional | Ya |
| Indosat | DNS | Ya |
| Smartfren | DNS Nasional | Ya |
| PT Cipta Informatika Cemeriang | DNS | Tidak |
| Oxygen | DNS Nasional | Tidak |
| Astinet | DNS Nasional | Ya |
| Lintasarta | DNS Nasional | Tidak |
| Citranet | DNS | Tidak |
| PT Metrasat | DNS Nasional | Ya |
| PT Pasifik Satelit Nusantara | DNS Nasional | Tidak |
| PT Artha Telekomindo | DNS | Tidak |
| PT Netciti Persada | DNS | Tidak |
| PT Hawk Teknologi Solusi | DNS | Tidak |
| PT Jaringanku Sarana Nusantara | DNS Nasional | Tidak |
| Padi Net | DNS Nasional | Tidak |
